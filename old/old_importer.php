<?php

ini_set('memory_limit', '2048M');

require 'Classes/aws/aws-autoloader.php';
require_once('Classes/PHPExcel.php');

//TODO - move these into config file
define("IMAGE_DOWNLOAD_PATH", "/tmp/importer/images/");
define("S3_PATH", "https://s3-ap-southeast-1.amazonaws.com/fk-staging-bucket/shop/listings/images/");

parse_str(implode('&', array_slice($argv, 1)), $_GET);


/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);

$objReader = PHPExcel_IOFactory::createReader('Excel2007');

$inputFileType = 'Excel2007';
$inputFileName = 'data.xlsx';
$sheetIndex = 0;

$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$sheetnames = $objReader->listWorksheetNames($inputFileName);
$objReader->setLoadSheetsOnly($sheetnames[$sheetIndex]);

echo "Loading Sheet data...\n";
$objPHPExcel = $objReader->load($inputFileName);
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
echo "Sheet data loaded.\n";

// remove first row
array_shift($sheetData);
$output = "";

$product_map = [
	"cloth" => [
		"attribute_set_name" => "Cloth Attribute Set",
		"configuration_key" => "cloth_color",
		"columns" => [
                	"cloth_material" => "G",
                	"cloth_type" => "H",
                	"cloth_color" => "I",
                	"cloth_size" => "J"
		],
	],
	"decal" => [
                "attribute_set_name" => "Decal Attribute Set",
                "configuration_key" => "decal_color",
		"columns" => [
			"decal_type" => "K",
			"decal_dimensions" => "L",
			"decal_color" => "M"
		],
	],
	"line" => [
                "attribute_set_name" => "Line Attribute Set",
                "configuration_key" => "line_color",
		"columns" => [
			"line_type" => "N",
			"line_lineclass" => "O",
			"line_length" => "P",
			"line_diameter" => "Q",
			"line_color" => "R"
		],
	],
	"reel" => [
                "attribute_set_name" => "Reel Attribute Set",
                "configuration_key" => "reel_handretrieve",
		"columns" => [
			"reel_weight" => "S",
			"reel_linecapacity" => "T",
			"reel_gearratio" => "U",
			"reel_ballbearing" => "V",
			"reel_maxdrag" => "W",
			"reel_handretrieve" => "X"
		],
	],
	"reel part" => [
                "attribute_set_name" => "Reel Part Attribute Set",
                "configuration_key" => "reelpart_color",
		"columns" => [
			"reelpart_part" => "Y",
			"reelpart_weight" => "Z",
			"reelpart_length" => "AA",
			"reelpart_diameter" => "AB",
			"reelpart_reeltype" => "AC",
			"reelpart_color" => "AD",
			"reelpart_features" => "AE"
		],
	],
	"storage" => [
                "attribute_set_name" => "Storage Attribute Set",
                "configuration_key" => "storage_color",
		"columns" => [
			"storege_type" => "AF",
			"storage_features" => "AG",
			"storage_dimensions" => "AH",
			"storage_size" => "AI",
			"storage_weight" => "AJ",
			"storage_color" => "AK",
			"storage_remarks" => "AL"
		],
	],
	"terminal" => [
                "attribute_set_name" => "Terminal Attribute Set",
                "configuration_key" => "terminal_size",
		"columns" => [
			"terminal_size" => "AM",
			"terminal_weight" => "AN",
			"terminal_strength" => "AO",
			"terminal_type" => "AP",
			"terminal_color" => "AQ",
			"terminal_quantityinpacket" => "AR",
			"terminal_length" => "AS",
			"terminal_diameter" => "AT"
		],
	],
	"tool" => [
		"attribute_set_name" => "Tool Attribute Set",
		"configuration_key" => "",
		"columns" => [
			"tool_dimensions" => "AU",
			"tool_color" => "AV",
			"tool_features" => "AW"
		],
	],
        "rod" => [
                "attribute_set_name" => "Rod Attribute Set",
		"configuration_key" => "",
                "columns" => [
                        "rod_type" => "AX",
                        "rod_length" => "AY",
                        "rod_weight" => "AZ",
			"rod_power" => "BA",
			"rod_lure" => "BB",
			"rod_line" => "BC",
			"rod_nbpieces" => "BD"
                ],
	],
        "lure" => [
                "attribute_set_name" => "Lure Attribute Set",
                "configuration_key" => "",
                "columns" => [
                        "lure_type" => "BE",
                        "lure_weight" => "BF",
			"lure_length" => "BG",
                        "lure_sound" => "BH"
                ],
        ]
];

$column_names = [
   0 => 'sku',
   1 => 'store_view_code',
   2 => 'attribute_set_code',
   3 => 'product_type',
   4 => 'categories',
   5 => 'product_websites',
   6 => 'name',
   7 => 'description',
   8 => 'short_description',
   9 => 'weight',
   10 => 'product_online',
   11 => 'tax_class_name',
   12 => 'visibility',
   13 => 'price',
   14 => 'special_price',
   15 => 'special_price_from_date',
   16 => 'special_price_to_date',
   17 => 'url_key',
   18 => 'meta_title',
   19 => 'meta_keywords',
   20 => 'meta_description',
   21 => 'base_image',
   22 => 'base_image_label',
   23 => 'small_image',
   24 => 'small_image_label',
   25 => 'thumbnail_image',
   26 => 'thumbnail_image_label',
   27 => 'swatch_image',
   28 => 'swatch_image_label',
   29 => 'created_at',
   30 => 'updated_at',
   31 => 'new_from_date',
   32 => 'new_to_date',
   33 => 'display_product_options_in',
   34 => 'map_price',
   35 => 'msrp_price',
   36 => 'map_enabled',
   37 => 'gift_message_available',
   38 => 'custom_design',
   39 => 'custom_design_from',
   40 => 'custom_design_to',
   41 => 'custom_layout_update',
   42 => 'page_layout',
   43 => 'product_options_container',
   44 => 'msrp_display_actual_price_type',
   45 => 'country_of_manufacture',
   46 => 'additional_attributes',
   47 => 'qty',
   48 => 'out_of_stock_qty',
   49 => 'use_config_min_qty',
   50 => 'is_qty_decimal',
   51 => 'allow_backorders',
   52 => 'use_config_backorders',
   53 => 'min_cart_qty',
   54 => 'use_config_min_sale_qty',
   55 => 'max_cart_qty',
   56 => 'use_config_max_sale_qty',
   57 => 'is_in_stock',
   58 => 'notify_on_stock_below',
   59 => 'use_config_notify_stock_qty',
   60 => 'manage_stock',
   61 => 'use_config_manage_stock',
   62 => 'use_config_qty_increments',
   63 => 'qty_increments',
   64 => 'use_config_enable_qty_inc',
   65 => 'enable_qty_increments',
   66 => 'is_decimal_divided',
   67 => 'website_id',
   68 => 'related_skus',
   69 => 'related_position',
   70 => 'crosssell_skus',
   71 => 'crosssell_position',
   72 => 'upsell_skus',
   73 => 'upsell_position',
   74 => 'additional_images',
   75 => 'additional_image_labels',
   76 => 'hide_from_product_page',
   77 => 'bundle_price_type',
   78 => 'bundle_sku_type',
   79 => 'bundle_price_view',
   80 => 'bundle_weight_type',
   81 => 'bundle_values',
   82 => 'bundle_shipment_type',
   83 => 'configurable_variations',
   84 => 'configurable_variation_labels',
   85 => 'associated_skus'
];

foreach ($column_names as $column_index => $column_name)
{
	$output .= $column_name . ',';
}

//Remove the last comma
$output = substr($output, 0, -1) . "\n";

if(count($sheetData) > 0)
{	
	$previousModel = $sheetData[0]['F']; //Model
	$previousItemType = $sheetData[0]['B'];
	$currentOutputArray = [];
	$configurableVariationsArray = [];

	//Cleanup sheetData for  any annoying CSV related characters
	foreach ($sheetData as $index => $row)
	{
		$safeData = [];
		foreach ($row as $rowIndex => $rowData)
		{
			$safeValue = str_replace('"', '""', $rowData); //change an any inner " to ""
			$safeValue = str_replace(',', '', $safeValue); //remove any dangerous comma that may break CSV format
			$safeValue = trim($safeValue); //Remove any heading / trailing space in values
			$row[$rowIndex] = $safeValue;
		}
		$sheetData[$index] = $row;
	}	

	$nbProcessed = 0;
	foreach($sheetData as $currentSheetRow)
	{
		$currentModel = $currentSheetRow['F']; //Model
		$item_type = strtolower($currentSheetRow['B']);

		if ($currentModel != $previousModel &&
			$product_map[$previousItemType]["configuration_key"] != "") //some item types will only generate simple products
		{
			//We have jumped to another product, create a configurable product with a copy of the last known item
			//Iannis
			$currentOutputArray[0] = '"' . str_replace('"', '', $currentOutputArray[0]) . '-1"'; //sku
			$currentOutputArray[3] = "\"configurable\""; //product_type
			$currentOutputArray[6] = $previousModel; //name
			$currentOutputArray[12] = "\"Catalog, Search\""; //visibility
			$currentOutputArray[17] = '"' . str_replace('"', '', $currentOutputArray[17] . '-' . $currentOutputArray[0]) . '-1"'; //url_key
			$currentOutputArray[27] = ""; //qty

			//Dumping the configurable_variations buffer
			$configurableVariations = '"';
			foreach ($configurableVariationsArray as $index => $value) {
                                $configurableVariations .= $value . "|";
                        }
                        $configurableVariations = substr($configurableVariations, 0, -1) . '"';
			$currentOutputArray[83] = $configurableVariations;
                        $configurableVariationsArray = [];

			foreach ($currentOutputArray as $index => $value) {
                        	$output .= $value . ",";
                	}
                	$output = substr($output, 0, -1) . "\n";
		}
		$previousModel = $currentModel;
		$previousItemType = $item_type;

		$currentOutputArray = [];
		foreach($column_names as $column_index => $column_name) 
		{
			$value = "";
			switch($column_index) {
				
				case 0: //sku
					$value = '"' .$currentSheetRow['A'] . '"'; //CUSTOM LABEL = sku
					break;
				case 1: //store_view_code
					$currentOutput = '';
					break;
				case 2: //attribute_set_code
					$value = '"' . $product_map[$item_type]["attribute_set_name"] . '"';
					break;
				case 3: //product_type
					$value = '"virtual"'; //Product is always virtual even when simple
					break;
				case 4: //categories
					$value = '"Default Category/' . ucwords(strtolower($currentSheetRow['B'])) .'"'; //Masterdata
					break;
				case 5: //product_websites
					$value = 'base';
					break;
				case 6: //name
					$value = '"'. $currentSheetRow['C'] . '"'; //ITEM
					break;
				case 7: //description
				case 8: //short_description
				case 9: //weight
					$value = '';
					break;
				case 10://product_online
					$value = '1';
					break;
				case 11: //tax_class_name
					$value = '"Taxable Goods"';
					break;
				case 12: //visibility
					$value = '"Not Visible Individually"'; //Default for virtual products
					if ($product_map[$item_type]["configuration_key"] == "") 
					{
						//Simple products should be searchable
						$value = "\"Catalog, Search\"";
					}
					break;
				case 13: //price
					$value = $currentSheetRow['BL']; //Price
					break;
				case 14: //special_price
				case 15: //special_price_from_date
				case 16: //special_price_to_date
					$value = '';
                                        break;
				case 17: //url_key
					$url_key = preg_replace('#[ -]+#', '-', $currentSheetRow['F']); //Model
                			$url_key = str_replace('"', '', $url_key);
					//Make SKU part of the url_key for simple products
					if ($product_map[$item_type]["configuration_key"] == "")
                                        {
                                                $url_key = $url_key . '-' . $currentSheetRow['A'];
                                        }

					$value = '"' . $url_key .'"';
					break; 
				case 18: //meta_title
				case 19: //meta_keywords
				case 20: //meta_description
					$value = '"'. $currentSheetRow['C']  .'"'; //ITEM
					break;
				case 21: //base_image
				case 23: //small_image
				case 25: //thumbnail_image
					$value ='"'.$currentSheetRow['D'].'"'; //Pic_URL
					//Retrieve the file if not already here - then rename it
					$path_parts = pathinfo($currentSheetRow['D']);
					//Format is path/to/sku-filename
					$s3FileName = $currentSheetRow['A'] . '-' . $path_parts['basename'];
					$localFileName = IMAGE_DOWNLOAD_PATH . $s3FileName;
					$value ='"' . S3_PATH . $s3FileName .'"';

					//Download image locally if flag is set
					if (isset ($_GET['fetchImages']) && $_GET['fetchImages'] == 1)
					{
						if (file_exists($localFileName) && filesize($localFileName) > 0)
						{
							//all good, file is already here - down the road we may have a flag to force download again
						}
						elseif (file_exists($localFileName) && filesize($localFileName) == 0)
						{
							//We have an issue - the file failed to be downloaded before. we should skip this product
							//TODO
						} 
						elseif (!file_exists($localFileName))
						{
							$fileContent = file_get_contents($currentSheetRow['D']);
							file_put_contents($localFileName, $fileContent); //if fileContent is FALSE, empty file created
							//All done for this column
						}
					}

					break;
				case 22: //base_image_label
				case 24: //small_image_label
				case 26: //thumbnail_image_label
				case 27: //swatch_image
				case 28: //swatch_image_label
					$value ='';
					break;
				case 29: //created_at
				case 30: //updated_at
					$value ='"'.date('h:i: a m/d/Y').'"';
					break;
				case 31: //new_from_date
				case 32: //new_to_date
				case 34: //map_price
				case 35: //msrp_price
				case 36: //map_enabled
					$value ='';
					break; 
				case 33: //display_product_options_in
					$value ='"Block after Info Column"';
					break;
				case 37: //gift_message_available
					$value ='"No"';
					break;
				case 38: //custom_design
				case 39: //custom_design_from
				case 40: //custom_design_to
				case 41: //custom_layout_update
				case 42: //page_layout
				case 43: //product_options_container
				case 44: //msrp_display_actual_price_type
				case 45: //country_of_manufacture
					$value = '';
                                        break;
				case 46: //additional_attributes
					$value = '"';
					foreach($product_map[$item_type]["columns"] as $attr => $columnIndex)
					{
						$value .= $attr . "=" . $currentSheetRow[$columnIndex] . ",";
					}
					$value = substr($value, 0, -1); //trim the last comma
					$value .= '"';
					break;
				case 47: //qty
					$value = $currentSheetRow['BK']; //Stock
					break;
				case 48: //out_of_stock_qty
					$value = '0';
					break;    
				case 49: //use_config_min_qty
					$value = '1';
					break;
				case 50: //is_qty_decimal
				case 51: //allow_backorders
					$value = '0';
					break;
				case 52: //use_config_backorders
				case 53: //min_cart_qty
				case 54: //use_config_min_sale_qty
					$value = '1';
					break;
				case 55: //max_cart_qty
					$value = '6';
					break;
				case 56: //use_config_max_sale_qty
				case 57: //is_in_stock
					$value = '1';
					break;
				case 58: //notify_on_stock_below
				case 59: //use_config_notify_stock_qty
				case 60: //manage_stock
				case 61: //use_config_manage_stock
				case 62: //use_config_qty_increments
				case 63: //qty_increments
				case 64: //use_config_enable_qty_inc
				case 65: //enable_qty_increments
				case 66: //is_decimal_divided
					$value = '1';
					break;
				case 67: //website_id
					$value = '0';
					break;
				case 68: //related_skus
				case 69: //related_position
				case 70: //crosssell_skus
				case 71: //crosssell_position
				case 72: //upsell_skus
				case 73: //upsell_position
				case 74: //additional_images
				case 75: //additional_image_labels
				case 76: //hide_from_product_page
				case 77: //bundle_price_type
				case 78: //bundle_sku_type
				case 79: //bundle_price_view
				case 80: //bundle_weight_type
				case 81: //bundle_values
				case 82: //bundle_shipment_type
					$value = '';
					break;
				case 83: //configurable_variations
					//Need to save the configuration variation for the configurable product
					$config_key = $product_map[$item_type]["configuration_key"];
					if ($config_key != "") {
						$configurableVariationsArray[] = "sku=" . $currentSheetRow['A'] . "," . $config_key . "=" . $currentSheetRow[$product_map[$item_type]["columns"][$config_key]];
					}
				case 84: //configurable_variation_labels
				case 85: //associated_skus
					$value = '';
					break;
				default:
				break;
			}
			$currentOutputArray[$column_index] = $value;
		}
		
		foreach ($currentOutputArray as $index => $value) {
			$output .= $value . ",";
		}
		$output = substr($output, 0, -1) . "\n";
		$nbProcessed++;
		echo "Processed " . $nbProcessed . " rows.\n";
	}
}


$today_datetime = date('Y-m-d-His'); 
//echo $output;
file_put_contents("shop_import_" . $today_datetime . ".csv", $output);
?>
