<?php
require 'src/bootstrap.php';

use League\Csv\Writer;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

$reader = ReaderFactory::create(Type::XLSX);
$csv = Writer::createFromString('');
$csvData = [];


$reader->open(__DIR__. '/import/cloth.xlsx');
$line = [];
foreach ($reader->getSheetIterator() as $sheet) {
    foreach ($sheet->getRowIterator() as $row) {
        // exclude first line
        if (in_array('PRODUCT DATA', $row) || in_array('CUSTOM LABEL', $row)) {
            continue;
        };
        //.... your code here
    }
}
//file_put_contents(__DIR__. '/export/cloth.csv', $csv->getContent());