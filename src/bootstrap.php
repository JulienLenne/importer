<?php
/**
 * Created by Julien Lenne.
 * User: JL
 *
 */

ini_set('memory_limit', '2048M');
require_once __DIR__ . '../vendor/autoload.php'; // Autoload files using Composer autoload